﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ActManager : MonoBehaviour
{
    public void on_prolog()
    {
        SceneManager.LoadScene("Prolog");
    }

    public void on_act1()
    {
        PrologManager.load_gather_intro();
    }

    public void on_act2()
    {
        GatherGameManager.next();
    }

    public void on_act3()
    {
        GatherGameManager.load_strauchschicht();
    }

    public void on_act4()
    {
        TreeBuilder.outro_dialog();
    }

    public void on_epilog()
    {
        EpilogManager.StartEpilog();
    }
}
