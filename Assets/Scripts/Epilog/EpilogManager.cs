using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EpilogManager : MonoBehaviour
{
    public static void StartEpilog()
    {
        SceneManager.LoadScene("Epilog");
    }

    public static void StartHabichtEndScreen()
    {
        SceneManager.LoadScene("HabichtEndScreen");
    }

    public static void StartKobelEndScreen()
    {
        SceneManager.LoadScene("KobelEndScreen");
    }

    public void on_habichtsnest()
    {
        StartHabichtEndScreen();
    }

    public void on_kobel()
    {
        StartKobelEndScreen();
    }
}
