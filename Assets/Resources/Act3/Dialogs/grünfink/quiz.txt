﻿[0]
question = Welches ist keine Schicht des Waldes ?
correct = 1
0 = Moosschicht
1 = Farnenschicht
2 = Wurzelschicht

[1]
question = Wie haben sich Frühblüher auf die Bedingungen des Waldes eingestellt?
correct = 2
0 = Frühblüher blühen erst im Herbst
1 = wachsen in große Höhen um das Licht erreichen zu können
2 = Sie speichern Energie in Zwiebeln/Knollen, um vor Blattaustrieb der Bäume zu blühen.