﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class QuizManager : MonoBehaviour
{
    public GameObject answer_button;

    public int remaining_lives;
    public int correct_answers_needed;

    private int correct_answers = 0;

    private int current_question_index = 0;

    private Quiz quiz_data = null;

    // Start is called before the first frame update
    void Start()
    {
        quiz_data = TreeBuilder.current_quiz();

        if (quiz_data == null)
        {
            Debug.Log("failed getting current quiz");
            return;
        }

        // reduce repetitiveness
        quiz_data.randomize_order();

        update_live_counter();
        update_question();
    }

    public void check_answer()
    {
        // get the event system handle
        var currentEventSystem = EventSystem.current;

        if (currentEventSystem is null)
        {
            Debug.Log("no event system found");
            return;
        }

        // get the button that called this event
        GameObject currentSelectedGameObject = currentEventSystem.currentSelectedGameObject;

        if (currentSelectedGameObject is null)
        {
            Debug.Log("no current game object selected");
            return;
        }

        AnswerInfo info = currentSelectedGameObject.GetComponent<AnswerInfo>();

        if (info is null)
        {
            Debug.Log("object has no component named AnswerInfo");
            return;
        }

        // correct answer
        if (info.answer_index == quiz_data.question(current_question_index).correct_answer_index)
        {
            correct_answers++;

            if (correct_answers == correct_answers_needed)
            {
                // DialogManager.StartDialog(new VoidFunction(TreeBuilder.load_climbing), TreeBuilder.current_dialog());
                TreeBuilder.process_correct_quiz();
            }

            update_feedback(true);
        }
        // wrong answer
        else
        {
            remaining_lives--;

            if (remaining_lives == 0)
            {
                TreeBuilder.load_climbing(false);
            }

            update_feedback(false);
        }

        current_question_index++;

        update_live_counter();
        update_question();
    }

    private void update_question()
    {
        Text text_field = GameObject.Find("Question").GetComponent<Text>();

        if (text_field is null)
        {
            Debug.Log("could not find question text field");
            return;
        }

        Question question = quiz_data.question(current_question_index);

        if (question is null)
        {
            Debug.Log("could not get question at: " + current_question_index);
            return;
        }

        text_field.text = question.question;

        Canvas canvas = GameObject.Find("Canvas").GetComponent<Canvas>();

        if (canvas is null)
        {
            Debug.Log("could not find Canvas");
            return;
        }

        Button.ButtonClickedEvent on_click = new Button.ButtonClickedEvent();
        on_click.AddListener(check_answer);

        Vector3 start_location = new Vector3(0.0f, 0.0f, 0.0f);
        Vector3 offset = new Vector3(0.0f, -60.0f, 0.0f);

        for (int i = 0; i < question.answers.Count; i++)
        {
            Vector3 position = start_location + (i * offset);

            GameObject game_object = Instantiate(answer_button, position, Quaternion.identity);

            if (game_object is null)
            {
                Debug.Log("could not create requested game object");
                return;
            }

            Button new_button = game_object.GetComponent<Button>();

            if (new_button is null)
            {
                Debug.Log("game object is not a button");
                return;
            }

            new_button.GetComponentInChildren<Text>().text = question.answers[i];

            // set canvas as the parent for our new button
            new_button.transform.SetParent(canvas.transform, true);
            new_button.transform.localPosition = position;

            new_button.onClick = on_click;

            AnswerInfo answer_info = game_object.GetComponent<AnswerInfo>();

            if (answer_info is null)
            {
                Debug.Log("game object does not have AnswerInfo component");
                return;
            }

            answer_info.answer_index = i;
        }
    }

    void update_live_counter()
    {
        Text live_counter = GameObject.Find("LiveCounter").GetComponent<Text>();
        live_counter.text = remaining_lives.ToString();
    }

    void update_feedback(bool correct)
    {
        Text feedback = GameObject.Find("Feedback").GetComponent<Text>();

        if (correct)
        {
            feedback.text = "Richtig!";
            feedback.color = Color.green;
        }
        else
        {
            feedback.text = "Falsch!";
            feedback.color = Color.red;
        }
    }
}
