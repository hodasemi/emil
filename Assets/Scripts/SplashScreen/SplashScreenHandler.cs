using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreenHandler : MonoBehaviour
{
    public void on_splash()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
