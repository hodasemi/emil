using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Text;
using UnityEngine;

class MetaSection
{
    public MetaSection(string header)
    {
        this.header = header;
        body = new Dictionary<string, string>();
    }

    public string header;
    public Dictionary<string, string> body;
}

public class Loader
{
    public static Dictionary<string, Dictionary<string, string>> load(string path)
    {
        // temporary object
        MetaSection meta_section = null;

        // our final return object
        Dictionary<string, Dictionary<string, string>> infos = new Dictionary<string, Dictionary<string, string>>();

        TextAsset text = Resources.Load<TextAsset>(path);

        if (text is null)
        {
            throw new FileNotFoundException(path);
        }

        string[] lines = text.text.Split('\n');

        foreach (string line in lines)
        {
            // trim any whitespaces
            var trimmed_line = line.Trim();

            // skip comments starting with '#' or empty lines
            if (string.IsNullOrEmpty(trimmed_line) || trimmed_line.StartsWith("#"))
            {
                continue;
            }
            // meta tags
            else if (trimmed_line.StartsWith("[") && trimmed_line.EndsWith("]"))
            {
                // get the actual text of the meta tag
                var meta = trimmed_line.Substring(1, trimmed_line.Length - 2);

                // if we already got a meta section push this into the infos map
                if (meta_section != null)
                {
                    infos.Add(meta_section.header, meta_section.body);
                }

                // create new meta section
                meta_section = new MetaSection(meta);
            }
            // body info
            else
            {
                string[] split = trimmed_line.Split('=');

                if (split.Length != 2)
                {
                    Debug.Log("wrong argument count (" + split.Length + "): " + trimmed_line + " in " + path);
                    continue;
                }

                string key = split[0].Trim();
                string value = split[1].Trim();

                meta_section.body.Add(key, value);
            }
        }

        // also push the last section
        if (meta_section != null)
        {
            infos.Add(meta_section.header, meta_section.body);
        }

        return infos;
    }
}