using UnityEngine;

public class RestoreInfo
{
    public RestoreInfo(Vector3 position, int index, int parent, int depth)
    {
        this.position = position;
        this.index = index;
        this.parent = parent;
        this.depth = depth;
    }

    public Vector3 position;
    public int depth;
    public int index;
    public int parent;
    public bool visited = false;

}