using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public enum GatherObjectType
{
    Player,
    Good,
    Bad
}

public class GatherObjectInfo : MonoBehaviour
{
    public Vector2 position;
    public GatherObjectType type;

    void OnCollisionEnter(Collision collision)
    {
        // look from the players view
        if (type == GatherObjectType.Player)
        {
            GameObject collider = collision.collider.gameObject;
            GatherObjectInfo other_info = collider.GetComponent<GatherObjectInfo>();

            GatherGameManager gather_manager = GameObject.Find("Canvas").GetComponent<GatherGameManager>();

            // if other object is also a gather object
            if (!(other_info is null))
            {
                if (other_info.type == GatherObjectType.Good)
                {
                    gather_manager.good_collision(collider);
                }
                else if (other_info.type == GatherObjectType.Bad)
                {
                    gather_manager.bad_collision(collider);
                }
            }
        }
    }
}