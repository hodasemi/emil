﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GatherGameManager : MonoBehaviour
{
    // public variables
    public Texture2D playerImage;
    public List<Texture2D> goodObjectImages;
    public List<Texture2D> badObjectImages;

    public GameObject gatherGameObject;

    public float objectDropSpeedRatio;
    public float playerSpeedRatio;

    public GameObject counterPrefab;
    public GameObject counterInfo;

    // spawn timings
    public float minSpawnTime;
    public float maxSpawnTime;

    // end screens
    public GameObject victoryText;
    public GameObject victoryButton;

    public GameObject defeatText;
    public GameObject defeatButton1;
    public GameObject defeatButton2;

    private List<GameObject> defeat_elements = new List<GameObject>();

    // spawn handling
    private float last_spawn_time;
    private float time_to_next_spawn;

    // player handling
    private GameObject player;

    // list of current objects
    private List<GameObject> images = new List<GameObject>();

    // gathering counters
    private int good_ones_needed;
    private int bad_ones_allowed;

    private float playerSpeed;
    private float objectDropSpeed;

    private bool enable_spawn;
    private bool end_screen;

    private bool playing;

    private GameObject good_counter;
    private GameObject good_counter_info;
    private GameObject bad_counter;
    private GameObject bad_counter_info;

    private void Start()
    {
        playing = false;
    }

    public void good_collision(GameObject collider)
    {
        collision(collider);
        good_ones_needed--;

        update_counters();
    }

    public void bad_collision(GameObject collider)
    {
        collision(collider);
        bad_ones_allowed--;

        update_counters();
    }

    private void Update()
    {
        // only update things if the user pressed the play button
        if (playing)
        {
            if (end_screen)
            {
                return;
            }

            GameObject canvas = GameObject.Find("Canvas");

            if (canvas is null)
            {
                return;
            }

            // check win condition
            if (good_ones_needed == 0)
            {
                clear_game_elements();

                GameObject victory_text = Instantiate(victoryText, canvas.transform);
                GameObject victory_button = Instantiate(victoryButton, canvas.transform);

                Button vb = victory_button.GetComponent<Button>();
                vb.onClick.AddListener(() => { post_game_dialog(); });

                end_screen = true;
            }

            if (bad_ones_allowed == 0)
            {
                clear_game_elements();

                GameObject defeat_text = Instantiate(defeatText, canvas.transform);
                GameObject defeat_restart = Instantiate(defeatButton2, canvas.transform);
                GameObject defeat_back = Instantiate(defeatButton1, canvas.transform);

                defeat_elements.Add(defeat_text);
                defeat_elements.Add(defeat_restart);
                defeat_elements.Add(defeat_back);

                Button dr = defeat_restart.GetComponent<Button>();
                dr.onClick.AddListener(() => { restart(); });

                Button db = defeat_back.GetComponent<Button>();
                db.onClick.AddListener(() => { back(); });

                end_screen = true;
            }

            // get frame time
            float frame_time = Time.deltaTime;

            Rect canvas_rect = canvas.GetComponent<RectTransform>().rect;

            if (!(player is null))
            {
                // update player position
                if (Input.GetAxis("Horizontal") < 0)
                {
                    GatherObjectInfo info = player.GetComponent<GatherObjectInfo>();
                    // TODO: bounds check
                    //player.transform.position.x -= (frame_time * playerSpeed);

                    info.position.x -= (frame_time * playerSpeed);

                    if (info.position.x < canvas_rect.xMin)
                    {
                        info.position.x = canvas_rect.xMin;
                    }

                    Vector3 pos = player.transform.localPosition;
                    pos.x = info.position.x;

                    player.transform.localPosition = pos;
                }
                else if (Input.GetAxis("Horizontal") > 0)
                {
                    GatherObjectInfo info = player.GetComponent<GatherObjectInfo>();
                    // TODO: bounds check
                    //player.transform.position.x += (frame_time * playerSpeed);

                    info.position.x += (frame_time * playerSpeed);

                    if (info.position.x > canvas_rect.xMax)
                    {
                        info.position.x = canvas_rect.xMax;
                    }

                    Vector3 pos = player.transform.localPosition;
                    pos.x = info.position.x;

                    player.transform.localPosition = pos;
                }
            }

            // update gather object positions
            for (int i = images.Count - 1; i >= 0; i--)
            {
                GameObject image = images[i];

                GatherObjectInfo info = image.GetComponent<GatherObjectInfo>();

                if (info is null)
                {
                    continue;
                }

                info.position.y -= (frame_time * objectDropSpeed);

                Rect image_rect = image.GetComponent<RectTransform>().rect;

                if (info.position.y < (canvas_rect.yMin + image_rect.yMin))
                {
                    images.RemoveAt(i);
                    Destroy(image);
                    continue;
                }

                Vector3 pos = image.transform.localPosition;
                pos.y = info.position.y;

                image.transform.localPosition = pos;
            }

            if (enable_spawn)
            {
                // spawn next element
                if ((last_spawn_time + time_to_next_spawn) < Time.time)
                {
                    last_spawn_time += time_to_next_spawn;

                    set_next_spawn_time();

                    int type = UnityEngine.Random.Range(0, 2);

                    if (type == 0)
                    {
                        create_good_one();
                    }
                    else
                    {
                        create_bad_one();
                    }
                }
            }
        }
    }

    public void start_playing()
    {
        playing = true;

        // delete info ui elements
        GameObject background = GameObject.Find("GameInfoBackground");

        if (background != null)
        {
            Destroy(background);
        }

        // setup game elements
        GameObject canvas = GameObject.Find("Canvas");

        if (canvas is null)
        {
            return;
        }

        Rect canvas_rect = canvas.GetComponent<RectTransform>().rect;

        float canvas_height = canvas_rect.yMax - canvas_rect.yMin;
        float canvas_width = canvas_rect.xMax - canvas_rect.xMin;

        playerSpeed = playerSpeedRatio * canvas_width;
        objectDropSpeed = objectDropSpeedRatio * canvas_height;

        last_spawn_time = Time.time;
        set_next_spawn_time();
        enable_spawn = true;
        end_screen = false;

        good_ones_needed = 5;
        bad_ones_allowed = 5;

        player = create_player();

        // create counters
        if (counterPrefab != null)
        {
            good_counter = Instantiate(counterPrefab, canvas.transform);
            bad_counter = Instantiate(counterPrefab, canvas.transform);
            good_counter_info = Instantiate(counterInfo, canvas.transform);
            bad_counter_info = Instantiate(counterInfo, canvas.transform);

            Rect counter_rect = counterPrefab.GetComponent<RectTransform>().rect;
            Rect counter_info_rect = counterInfo.GetComponent<RectTransform>().rect;

            // move bad counter
            Vector3 bad_counter_pos = bad_counter.transform.localPosition;
            bad_counter_pos.y -= counter_rect.height;
            bad_counter.transform.localPosition = bad_counter_pos;

            update_counters();

            // set good counter info text and position
            good_counter_info.GetComponent<Text>().text = "Gute:";
            Vector3 good_counter_pos = good_counter.transform.localPosition;
            good_counter_pos.x += counter_rect.xMin - counter_info_rect.xMax;
            good_counter_info.transform.localPosition = good_counter_pos;

            // set bad counter info text and position
            bad_counter_info.GetComponent<Text>().text = "Schlechte:";
            bad_counter_pos.x += counter_rect.xMin - counter_info_rect.xMax;
            bad_counter_info.transform.localPosition = bad_counter_pos;
        }
    }

    private void update_counters()
    {
        good_counter.GetComponentInChildren<Text>().text = good_ones_needed.ToString();
        bad_counter.GetComponentInChildren<Text>().text = bad_ones_allowed.ToString();
    }

    private void clear_game_elements()
    {
        enable_spawn = false;
        Destroy(player);
        player = null;

        foreach (GameObject image in images)
        {
            Destroy(image);
        }

        images.Clear();
    }

    private void set_next_spawn_time()
    {
        time_to_next_spawn = Random.Range(minSpawnTime, maxSpawnTime);
    }

    private GameObject create_player()
    {
        GameObject canvas = GameObject.Find("Canvas");

        if (canvas is null)
        {
            return null;
        }

        Rect canvas_rect = canvas.GetComponent<RectTransform>().rect;

        if (gatherGameObject is null)
        {
            return null;
        }

        Rect gather_game_object_rect = gatherGameObject.GetComponent<RectTransform>().rect;
        float ggo_height = gather_game_object_rect.yMax - gather_game_object_rect.yMin;
        float canvas_height = canvas_rect.yMax - canvas_rect.yMin;

        Vector3 position = new Vector3(0.0f, canvas_rect.yMin + (ggo_height + (canvas_height / 25.0f)), 0.0f);

        GameObject obj = create_gather_game_object(playerImage, position);

        if (obj is null)
        {
            return null;
        }

        GatherObjectInfo goi = obj.GetComponent<GatherObjectInfo>();
        goi.type = GatherObjectType.Player;

        return obj;
    }

    private GameObject create_good_one()
    {
        Texture2D good_texture = goodObjectImages[UnityEngine.Random.Range(0, goodObjectImages.Count)];

        GameObject obj = create_gather_game_object(good_texture, gather_object_position());

        if (obj is null)
        {
            return null;
        }

        GatherObjectInfo goi = obj.GetComponent<GatherObjectInfo>();
        goi.type = GatherObjectType.Good;

        images.Add(obj);

        return obj;
    }

    private GameObject create_bad_one()
    {
        Texture2D bad_texture = badObjectImages[UnityEngine.Random.Range(0, badObjectImages.Count)];

        GameObject obj = create_gather_game_object(bad_texture, gather_object_position());

        if (obj is null)
        {
            return null;
        }

        GatherObjectInfo goi = obj.GetComponent<GatherObjectInfo>();
        goi.type = GatherObjectType.Bad;

        images.Add(obj);

        return obj;
    }

    private Vector3 gather_object_position()
    {
        GameObject canvas = GameObject.Find("Canvas");

        if (canvas is null)
        {
            return new Vector3();
        }

        Rect canvas_rect = canvas.GetComponent<RectTransform>().rect;

        if (gatherGameObject is null)
        {
            return new Vector3();
        }

        Rect gather_game_object_rect = gatherGameObject.GetComponent<RectTransform>().rect;
        float ggo_height = gather_game_object_rect.yMax - gather_game_object_rect.yMin;
        float ggo_width = gather_game_object_rect.xMax - gather_game_object_rect.xMin;
        float canvas_width = canvas_rect.xMax - canvas_rect.xMin;

        float space = canvas_width / 20.0f;

        return new Vector3(
            UnityEngine.Random.Range(canvas_rect.xMin + space, canvas_rect.xMax - (space + ggo_width)),
            canvas_rect.yMax + ggo_height,
            0.0f
        );
    }

    private GameObject create_gather_game_object(Texture2D image, Vector3 position)
    {
        // get canvas
        GameObject canvas = GameObject.Find("Canvas");

        if (canvas is null)
        {
            return null;
        }

        // create gather game object
        GameObject gather_game_object = Instantiate(gatherGameObject, position, Quaternion.identity);

        if (gather_game_object is null)
        {
            return null;
        }

        // set image
        set_image(gather_game_object, image);

        // set parent
        gather_game_object.transform.SetParent(canvas.transform);

        // set local_position
        gather_game_object.transform.localPosition = position;

        // set position to info (more precise position calculation)
        GatherObjectInfo goi = gather_game_object.GetComponent<GatherObjectInfo>();

        if (goi is null)
        {
            return null;
        }

        goi.position = new Vector2(position.x, position.y);

        return gather_game_object;
    }

    private void set_image(GameObject game_object, Texture2D image)
    {
        Image handle = game_object.GetComponent<Image>();
        handle.overrideSprite = Sprite.Create(image, new Rect(0.0f, 0.0f, image.width, image.height), new Vector2(0.5f, 0.5f));
    }

    private void collision(GameObject collider)
    {
        for (int i = 0; i < images.Count; i++)
        {
            if (images[i] == collider)
            {
                images.RemoveAt(i);
                Destroy(collider);
                break;
            }
        }
    }

    private void back()
    {
        PrologManager.load_gather_intro();
    }

    public static void post_game_dialog()
    {
        DialogManager.StartDialog(new VoidFunction(next), new Dialog("Act1", "PostGameDialog"));
    }

    public static void next()
    {
        DialogManager.StartDialog(new VoidFunction(load_strauchschicht), new Dialog("Act2", "RabbitDialog"));
    }

    public static void load_strauchschicht()
    {
        DialogManager.StartDialog(new VoidFunction(TreeBuilderInfo.start_climbing_info), new Dialog("Act3/", "PreClimbDialog"));
    }

    private void restart()
    {
        foreach (GameObject obj in defeat_elements)
        {
            Destroy(obj);
        }

        defeat_elements.Clear();

        Start();
    }
}
