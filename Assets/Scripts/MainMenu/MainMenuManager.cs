﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public void on_play()
    {
        SceneManager.LoadScene("Prolog");
    }

    public void on_acts()
    {
        SceneManager.LoadScene("KapitelAuswahl");
    }
}
