﻿[Start]
Greeting = Das war ja ein riesiger Greifvogel. Wir haben dir zu danken für deine Hilfe, Hase.
[a]
b = Puuh, ganz schön gefährlich hier in der … Wie nennt man das hier eigentlich? 
[c]
d = Und deshalb ist es hier so dunkel? 
[e]
f = Lass uns lieber schnell aufbrechen: wir Eichhörnchen sind nicht nachtaktiv und können im Dunkeln nur schlecht sehen. Vielen Dank nochmal, Hase. Bestimmt sieht man sich mal wieder. 
[g] 
End = Auf Wiedersehen! 

