using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TreeBuilderInfo : MonoBehaviour
{
    public static void start_climbing_info()
    {
        SceneManager.LoadScene("ClimbingInfo");
    }

    public void start_playing()
    {
        TreeBuilder.load_climbing(false);
    }
}
