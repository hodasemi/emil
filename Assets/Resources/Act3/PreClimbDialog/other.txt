[Greeting]
Answer = Ich habe dir doch versprochen, dass ich dich zurückbringe. Aber von hier unten sehen wir nichts.  Du musst nach oben klettern.
[Doubt]
Question = Wenn du nach Hause möchtest, musst du das Wohl oder Übel jetzt lernen. Du wirst auf deinem Weg nach oben sicher einige Tiere kennenlernen, finde heraus ob sie dir Helfen können. Du schaffst das! 