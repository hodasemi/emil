using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HabichtManager : MonoBehaviour
{
    public float duration;
    private float start_time;

    private void Start()
    {
        start_time = Time.time;
    }

    private void Update()
    {
        if (Time.time >= start_time + duration)
        {
            EpilogManager.StartEpilog();
        }
    }
}
