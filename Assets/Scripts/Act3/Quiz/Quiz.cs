﻿using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

public class Question
{
    public Question()
    {
        correct_answer_index = 0;
        answers = new List<string>();
        question = "";
    }

    public int correct_answer_index;
    public List<string> answers;
    public string question;
}

public class Quiz
{
    List<Question> questions;

    public Quiz(string dir, string name)
    {
        questions = new List<Question>();

        // prepare path
        string path = dir.Trim('/') + "/" + name + "/quiz";

        Dictionary<string, Dictionary<string, string>> quiz_data =
            new Dictionary<string, Dictionary<string, string>>();

        try
        {
            quiz_data = Loader.load(path);
        }
        catch (DirectoryNotFoundException)
        {
            Debug.Log(path + " not found");
        }

        int index = 0;
        Dictionary<string, string> question = null;

        while (quiz_data.TryGetValue(index.ToString(), out question))
        {
            // parse quiz data
            Question quiz = new Question();
            quiz.question = question["question"];
            quiz.correct_answer_index = Int32.Parse(question["correct"]);

            int question_index = 0;
            string answer = null;

            while (question.TryGetValue(question_index.ToString(), out answer))
            {
                quiz.answers.Add(answer);
                question_index++;
            }

            // push the current question
            questions.Add(quiz);

            // check for next question
            index++;
        }
    }

    public Question question(int index)
    {
        if (index < 0)
        {
            return questions[0];
        }

        if (index >= questions.Count)
        {
            return questions[questions.Count - 1];
        }

        return questions[index];
    }

    public void randomize_order()
    {
        questions.Shuffle();
    }
}