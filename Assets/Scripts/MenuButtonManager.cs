using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuButtonManager : MonoBehaviour
{
    public GameObject dummyAudioInput;
    public GameObject slider;

    private static GameObject internal_dummy_audio = null;
    private static GameObject internal_slider_pref = null;
    private static AudioSource audio_source = null;


    private static GameObject internal_slider = null;

    private void Start()
    {
        if (dummyAudioInput != null)
        {
            if (internal_dummy_audio is null)
            {
                internal_dummy_audio = Instantiate(dummyAudioInput);
                DontDestroyOnLoad(internal_dummy_audio);

                audio_source = internal_dummy_audio.GetComponent<AudioSource>();
                audio_source.clip = Resources.Load("song") as AudioClip;
                audio_source.loop = true;
                audio_source.volume = 0.2f;
                audio_source.Play();
            }
        }

        if (slider != null)
        {
            if (internal_slider_pref is null)
            {
                internal_slider_pref = slider;
            }
        }
    }

    public void on_audio()
    {
        if (internal_slider is null)
        {
            GameObject canvas = GameObject.Find("Canvas");
            internal_slider = Instantiate(internal_slider_pref, canvas.transform);

            internal_slider.transform.SetParent(canvas.transform);

            Slider slider_handle = internal_slider.GetComponentInChildren<Slider>();

            slider_handle.value = audio_source.volume;
            slider_handle.onValueChanged.AddListener(delegate { slider_event(); });
        }
        else
        {
            Destroy(internal_slider);
            internal_slider = null;
        }
    }

    public void on_home()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void slider_event()
    {
        Slider slider_handle = internal_slider.GetComponentInChildren<Slider>();

        audio_source.volume = slider_handle.value;
    }
}
