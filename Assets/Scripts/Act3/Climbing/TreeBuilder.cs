﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

class DataTuple
{
    public DataTuple(string dir, string name)
    {
        this.dialog = new Dialog(dir, name);
        this.quiz = new Quiz(dir, name);
    }

    public Dialog dialog;
    public Quiz quiz;
}

public class TreeBuilder : MonoBehaviour
{
    public GameObject visited_button;
    public GameObject unvisited_button;

    public int max_children;

    public int y_spacing;

    public int max_depth;

    private static int m_max_depth = 0;

    private static List<RestoreInfo> tree = new List<RestoreInfo>();

    public static int current_index = 0;
    public static int parent_index = -1;
    private static List<DataTuple> data_tuples = new List<DataTuple>();

    // returns the quiz with at parent index
    public static Quiz current_quiz()
    {
        if (parent_index < 0)
        {
            Debug.Log("requested quiz at " + parent_index);
            return null;
        }

        // check if present
        if (data_tuples.Count == 0)
        {
            return null;
        }

        int access_index = parent_index;

        // bounds check
        if (access_index >= data_tuples.Count)
        {
            access_index = data_tuples.Count - 1;
        }

        return data_tuples[access_index].quiz;
    }

    // returns the dialog at current index
    public static Dialog current_dialog()
    {
        if (current_index < 0)
        {
            Debug.Log("requested dialog at " + current_index);
            return null;
        }

        // check if present
        if (data_tuples.Count == 0)
        {
            return null;
        }

        int access_index = current_index;

        // bounds check
        if (current_index >= data_tuples.Count)
        {
            access_index = data_tuples.Count - 1;
        }

        return data_tuples[access_index].dialog;
    }

    private void Start()
    {
        m_max_depth = max_depth;

        GameObject canvas = GameObject.Find("Canvas");

        if (canvas is null)
        {
            return;
        }

        // inital setup
        if (tree.Count == 0)
        {
            Rect canvas_rect = canvas.GetComponent<RectTransform>().rect;

            // add root
            tree.Add(new RestoreInfo(new Vector3(0.0f, canvas_rect.yMin + 40.0f, 0.0f), 0, -1, 0));

            set_persistent_visited();
        }

        // load data
        if (data_tuples.Count == 0)
        {
            string list_path = "Act3/Dialogs/list";

            TextAsset test = Resources.Load<TextAsset>(list_path);
            string[] lines = test.text.Split('\n');

            foreach (string line in lines)
            {
                string directory = line.Trim();

                if (directory != "")
                {
                    data_tuples.Add(new DataTuple("Act3/Dialogs/", directory));
                }
            }
        }

        restore(canvas);
    }

    private void restore(GameObject canvas)
    {
        if (tree.Count == 0)
        {
            return;
        }

        Button.ButtonClickedEvent on_click = new Button.ButtonClickedEvent();
        on_click.AddListener(press_event);

        foreach (RestoreInfo restore_info in tree)
        {
            GameObject game_object = restore_button(restore_info, canvas, on_click);

            NodeInfo node_info = game_object.GetComponent<NodeInfo>();

            node_info.visited = restore_info.visited;
        }
    }

    public void press_event()
    {
        // get the event system handle
        var currentEventSystem = EventSystem.current;

        if (currentEventSystem == null)
        {
            Debug.Log("no event system found");
            return;
        }

        // get the button that called this event
        GameObject currentSelectedGameObject = currentEventSystem.currentSelectedGameObject;

        if (currentSelectedGameObject == null)
        {
            Debug.Log("no current game object selected");
            return;
        }

        NodeInfo current_info = currentSelectedGameObject.GetComponent<NodeInfo>();

        if (current_info is null)
        {
            Debug.Log("current game object does not implement NodeInfo");
            return;
        }

        Debug.Log("Current Depth: " + current_info.depth);

        // if (current_info.depth == max_depth)
        // {
        //     DialogManager.StartDialog(new VoidFunction(BaumschichtManager.finish_game), new Dialog("Act4", "OutroDialog"));
        //     return;
        // }

        current_index = current_info.index;
        parent_index = tree[current_index].parent;

        if (!current_info.visited)
        {
            SceneManager.LoadScene("Quiz");
        }
        else
        {
            void climb()
            {
                load_climbing(true);
            }

            DialogManager.StartDialog(new VoidFunction(climb), current_dialog());
        }
    }

    void create_children(GameObject game_object, NodeInfo parent_info)
    {
        Button parent_button = game_object.GetComponent<Button>();

        if (parent_button is null)
        {
            Debug.Log("current game object is not a button");
            return;
        }

        GameObject canvas = GameObject.Find("Canvas");

        if (canvas is null)
        {
            return;
        }

        Rect canvas_rect = canvas.GetComponent<RectTransform>().rect;

        int spacing = (int)canvas_rect.width / 10;
        int left_bound = (int)canvas_rect.xMin + spacing;
        int right_bound = (int)canvas_rect.xMax - spacing;

        // calculate new button position
        Vector3 position = game_object.transform.localPosition;
        Vector3 offset = new Vector3(0, y_spacing, 0);

        // TODO: make sure the buttons do not overshadow themselves
        // randomize the amount of buttons created
        System.Random rng = new System.Random();
        int amount = rng.Next(1, max_children);

        int x_base = rng.Next(-200, -50);
        int x_offset = rng.Next(100, 250);

        for (int i = 0; i < amount; i++)
        {
            offset.x = x_base + (i * x_offset);

            // TODO: a lot of bounds checking (screen borders and neighbours)

            Vector3 new_position = position + offset;

            if (new_position.x > right_bound)
            {
                new_position.x = right_bound;
            }

            if (new_position.x < left_bound)
            {
                new_position.x = left_bound;
            }

            create_new_button(new_position, canvas, parent_button.onClick, parent_info.index);
        }
    }

    public static void load_climbing(bool success)
    {
        if (success)
        {
            set_persistent_visited();
        }

        SceneManager.LoadScene("Climbing");
    }

    GameObject restore_button(
        RestoreInfo restore_info,
        GameObject canvas,
        Button.ButtonClickedEvent on_click
    )
    {
        GameObject game_object = null;

        if (restore_info.visited)
        {
            game_object = Instantiate(visited_button, restore_info.position, Quaternion.identity);
        }
        else
        {
            game_object = Instantiate(unvisited_button, restore_info.position, Quaternion.identity);
        }


        if (game_object is null)
        {
            Debug.Log("could not create requested game object");
            return null;
        }

        Button new_button = game_object.GetComponent<Button>();

        if (new_button is null)
        {
            Debug.Log("game object is not a button");
            return null;
        }

        // set canvas as the parent for our new button
        new_button.transform.SetParent(canvas.transform, true);
        new_button.transform.localPosition = restore_info.position;

        // set the same button event
        new_button.onClick = on_click;

        NodeInfo node_info = game_object.GetComponent<NodeInfo>();

        if (node_info is null)
        {
            Debug.Log("game object does not have NodeInfo component");
            return null;
        }

        node_info.index = restore_info.index;
        node_info.depth = restore_info.depth;

        return game_object;
    }

    void create_new_button(
        Vector3 position,
        GameObject canvas,
        Button.ButtonClickedEvent on_click,
        int parent_index
    )
    {
        GameObject game_object = Instantiate(unvisited_button, position, Quaternion.identity);

        if (game_object is null)
        {
            Debug.Log("could not create requested game object");
            return;
        }

        Button new_button = game_object.GetComponent<Button>();

        if (new_button is null)
        {
            Debug.Log("game object is not a button");
            return;
        }

        // set canvas as the parent for our new button
        new_button.transform.SetParent(canvas.transform, true);
        new_button.transform.localPosition = position;

        // set the same button event
        new_button.onClick = on_click;

        NodeInfo node_info = game_object.GetComponent<NodeInfo>();

        if (node_info is null)
        {
            Debug.Log("game object does not have NodeInfo component");
            return;
        }

        node_info.index = tree.Count;

        tree.Add(new RestoreInfo(position, tree.Count, parent_index, tree[parent_index].depth + 1));
    }

    // returns false if the element was already visited
    private static void set_persistent_visited()
    {
        foreach (RestoreInfo info in tree)
        {
            if (info.index == current_index)
            {
                if (!info.visited)
                {
                    info.visited = true;
                    add_children(tree[current_index]);
                }

                break;
            }
        }
    }

    private static void add_children(RestoreInfo parent)
    {
        GameObject canvas = GameObject.Find("Canvas");

        if (canvas is null)
        {
            return;
        }

        Rect canvas_rect = canvas.GetComponent<RectTransform>().rect;

        // calculate new button position
        Vector3 position = tree[current_index].position;
        Vector3 offset = new Vector3(0, canvas_rect.height / 5.0f, 0);

        // TODO: make sure the buttons do not overshadow themselves
        // randomize the amount of buttons created
        System.Random rng = new System.Random();
        int amount = rng.Next(1, 4);

        int x_base = rng.Next(-200, -50);
        int x_offset = rng.Next(100, 250);

        for (int i = 0; i < amount; i++)
        {
            offset.x = x_base + (i * x_offset);

            tree.Add(new RestoreInfo(position + offset, tree.Count, current_index, parent.depth + 1));
        }
    }

    public static void process_correct_quiz()
    {
        if (tree[current_index].depth == m_max_depth)
        {
            outro_dialog();
        }
        else
        {
            void climb()
            {
                load_climbing(true);
            }

            DialogManager.StartDialog(new VoidFunction(climb), TreeBuilder.current_dialog());
        }
    }

    public static void outro_dialog()
    {
        DialogManager.StartDialog(new VoidFunction(EpilogManager.StartEpilog), new Dialog("Act4", "OutroDialog"));
    }
}
