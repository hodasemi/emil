using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DialogType
{
    Squirrel,
    Other,
}

public class DialogChoice
{
    public DialogChoice(string target, string text, DialogType type)
    {
        this.target = target;
        this.text = text;
        this.type = type;
    }

    public string target;
    public string text;
    public DialogType type;
}

public class Dialog
{
    private Dictionary<string, Dictionary<string, string>> squirrel_text;
    private Dictionary<string, Dictionary<string, string>> other_text;

    private Texture2D background;
    private Texture2D squirrel;
    private Texture2D other;

    // debug purposes
    public string name;

    public Dialog(string dir, string name)
    {
        this.name = name;

        // prepare common path
        string trimmed_dir = dir.Trim('/');

        string base_dir = null;

        if (trimmed_dir == "")
        {
            base_dir = name + "/";
        }
        else
        {
            base_dir = trimmed_dir + "/" + name + "/";
        }

        // prepare dialog file paths
        string squirrel_path = base_dir + "squirrel";
        string other_path = base_dir + "other";

        // load dialog files
        try
        {
            squirrel_text = Loader.load(squirrel_path);
        }
        catch (DirectoryNotFoundException)
        {
            Debug.Log(squirrel_path + " not found");
        }

        try
        {
            other_text = Loader.load(other_path);
        }
        catch (DirectoryNotFoundException)
        {
            Debug.Log(other_path + " not found");
        }

        // ugly workaround incoming!
        string[] tmp = base_dir.Split(new string[] { "Resources/" }, StringSplitOptions.None);
        string suffix = tmp[tmp.Length - 1];

        // load background texture
        background = Resources.Load(suffix + "background") as Texture2D;

        if (background is null)
        {
            Debug.Log("could not find background: " + suffix + "background");
            return;
        }

        // load squirrel texture
        squirrel = Resources.Load(suffix + "squirrel_pic") as Texture2D;

        if (squirrel is null)
        {
            Debug.Log("could not find squirrel: " + suffix + "squirrel");
            return;
        }

        // load other texture
        other = Resources.Load(suffix + "other_pic") as Texture2D;

        if (other is null)
        {
            Debug.Log("could not find other: " + suffix + "other");
            return;
        }
    }

    public Texture2D get_background()
    {
        return background;
    }

    public Texture2D get_squirrel()
    {
        return squirrel;
    }

    public Texture2D get_other()
    {
        return other;
    }

    public List<DialogChoice> get_start()
    {
        return get_meta("Start");
    }

    public List<DialogChoice> get_meta(string meta_tag)
    {
        List<DialogChoice> dialog_list = new List<DialogChoice>();

        Dictionary<string, string> meta_section;

        if (squirrel_text.TryGetValue(meta_tag, out meta_section))
        {
            dialog_list = dictionary_into_list(meta_section, DialogType.Squirrel);
        }
        else if (other_text.TryGetValue(meta_tag, out meta_section))
        {
            dialog_list = dictionary_into_list(meta_section, DialogType.Other);
        }
        else
        {
            Debug.Log("Meta (" + meta_tag + ") for " + name + ", could not be resolved!");
        }

        return dialog_list;
    }

    private List<DialogChoice> dictionary_into_list(Dictionary<string, string> dict, DialogType type)
    {
        List<DialogChoice> list = new List<DialogChoice>();

        foreach (KeyValuePair<string, string> entry in dict)
        {
            list.Add(new DialogChoice(entry.Key, entry.Value, type));
        }

        return list;
    }
}