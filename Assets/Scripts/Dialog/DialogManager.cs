﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public delegate void VoidFunction();

public class DialogManager : MonoBehaviour
{
    public Button chat_box_squirrel;
    public Button chat_box_other;


    private static VoidFunction next_call = null;

    private static Dialog dialog_data = null;

    public static void StartDialog(VoidFunction next, Dialog dialog)
    {
        if (next != null)
        {
            next_call = next;
        }

        if (dialog != null)
        {
            dialog_data = dialog;
        }

        SceneManager.LoadScene("Dialog");
    }

    // Start is called before the first frame update
    void Start()
    {
        if (dialog_data == null)
        {
            Debug.Log("failed getting current dialog");
            return;
        }

        set_image("Background", dialog_data.get_background());
        set_image("Squirrel", dialog_data.get_squirrel());
        set_image("Other", dialog_data.get_other());

        create_chat_buttons(dialog_data.get_start());
    }

    private void set_image(string name, Texture2D image)
    {
        if (image is null)
        {
            Debug.Log("could not set " + name);
            return;
        }

        Image handle = transform.Find(name).gameObject.GetComponent<Image>();
        handle.overrideSprite = Sprite.Create(image, new Rect(0.0f, 0.0f, image.width, image.height), new Vector2(0.5f, 0.5f));
    }

    private List<GameObject> get_all_childs()
    {
        List<GameObject> list = new List<GameObject>();

        for (int i = 0; i < transform.childCount; i++)
        {
            list.Add(transform.GetChild(i).gameObject);
        }

        return list;
    }

    private void create_chat_buttons(List<DialogChoice> dialog)
    {
        Canvas canvas = transform.gameObject.GetComponent<Canvas>();

        if (canvas is null)
        {
            Debug.Log("Canvas not found");
        }

        Rect canvas_rect = canvas.GetComponent<RectTransform>().rect;
        Rect chat_rect = chat_box_squirrel.GetComponent<RectTransform>().rect;

        int padding = 30;

        int i = dialog.Count - 1;
        int start_y = (int)canvas_rect.yMin - (int)chat_rect.yMin + padding;

        foreach (DialogChoice dialog_option in dialog)
        {
            Button new_button = null;

            if (dialog_option.type == DialogType.Squirrel)
            {
                new_button = Instantiate(chat_box_squirrel, canvas.transform).GetComponent<Button>();
            }
            else
            {
                new_button = Instantiate(chat_box_other, canvas.transform).GetComponent<Button>();
            }

            Rect chat_box_rect = new_button.GetComponent<RectTransform>().rect;

            int button_height = (int)chat_box_rect.height;

            Vector3 position = new Vector3(0, start_y + (i * button_height) + (i * padding), 0);

            new_button.transform.localPosition = position;

            new_button.GetComponentInChildren<Text>().text = dialog_option.text;
            ChatBoxInfo info = new_button.GetComponent<ChatBoxInfo>();

            info.target = dialog_option.target;
            info.type = dialog_option.type;

            new_button.onClick.AddListener(next_chat);

            i--;
        }
    }

    private void clear_chat_buttons()
    {
        List<GameObject> children = get_all_childs();

        foreach (GameObject child in children)
        {
            var chat_box = child.GetComponent<ChatBoxInfo>();

            if (chat_box != null)
            {
                Destroy(child);
            }
        }
    }

    private void next_chat()
    {
        Button event_button = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();

        ChatBoxInfo info = event_button.GetComponent<ChatBoxInfo>();

        if (info.target == "End")
        {
            next_call();
            return;
        }

        clear_chat_buttons();
        create_chat_buttons(dialog_data.get_meta(info.target));
    }
}
