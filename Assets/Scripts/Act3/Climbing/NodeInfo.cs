﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeInfo : MonoBehaviour
{
    public int depth = -1;
    public int index = -1;
    public bool visited = false;
}
