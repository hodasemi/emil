﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
enum Phase
{
    Dropping,
    Pausing,
}

public class PrologManager : MonoBehaviour
{
    public float DropDuration;
    public float PauseDuration;

    private Vector3 start_position;
    private Vector3 target_position;

    private float start_time;

    private Phase phase;

    private void Start()
    {
        GameObject canvas = GameObject.Find("Canvas");

        if (canvas is null)
        {
            Debug.Log("canvas not found");
            return;
        }

        Rect canvas_rect = canvas.GetComponent<RectTransform>().rect;

        GameObject baby_squirrel = GameObject.Find("BabySquirrel");

        if (baby_squirrel is null)
        {
            Debug.Log("baby squirrel not found");
            return;
        }

        start_position = baby_squirrel.transform.localPosition;

        target_position = new Vector3(start_position.x, canvas_rect.yMin - (canvas_rect.height / 2.0f), 0.0f);

        phase = Phase.Dropping;
        start_time = Time.time;
    }

    private void Update()
    {
        // baby squirrel drops from the tree
        if (phase == Phase.Dropping)
        {
            GameObject baby_squirrel = GameObject.Find("BabySquirrel");

            if (baby_squirrel is null)
            {
                Debug.Log("baby squirrel not found");
                return;
            }

            if (Time.time >= (start_time + DropDuration))
            {
                phase = Phase.Pausing;
                start_time = start_time + DropDuration;
                baby_squirrel.transform.localPosition = target_position;
                return;
            }

            float time_diff = Time.time - start_time;
            float inter = time_diff / DropDuration;

            Vector3 new_pos = start_position + (target_position - start_position) * inter;

            baby_squirrel.transform.localPosition = new_pos;

            Quaternion quat = baby_squirrel.transform.localRotation;
            quat.z = -(inter);
            baby_squirrel.transform.localRotation = quat;
        }
        // "simulate" out of screen dropping, by a waitung phase
        else if (phase == Phase.Pausing)
        {
            // if waiting is over, go to a welcome dialog between baby and squirrel
            if (Time.time >= (start_time + DropDuration))
            {
                DialogManager.StartDialog(new VoidFunction(load_gather_intro), new Dialog("Act0", "IntroDialog"));
            }
        }
    }

    public static void load_gather_intro()
    {
        DialogManager.StartDialog(new VoidFunction(load_gather_game), new Dialog("Act1", "GatherIntroDialog"));
    }

    public static void load_gather_game()
    {
        SceneManager.LoadScene("GatherGame");
    }
}
